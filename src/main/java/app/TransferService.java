package app;

import java.lang.Thread;

public class TransferService implements Runnable {

    private Amount amount;
    private Balance balance;

    public TransferService(Amount amount, Balance balance) {
        this.amount = amount;
        this.balance = balance;
    }

    public void run() {
        try {
            //The below line is a service delay simulator. Do not change.
            Thread.sleep(1000);
            this.balance.value = this.balance.value - this.amount.value;
            System.out.println("[i] Successfully transfered: " + this.amount.value);
            System.out.println("[i] Your new balance is: " + this.balance.value);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
}
